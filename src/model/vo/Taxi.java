package model.vo;

import model.data_structures.Queue;

import model.data_structures.ListaJSON;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	private String idTaxi;
	private String compania; 
	private ListaJSON<Servicio> serviciosTaxi;
	private double ingresos; 
	private double recorrido; 
	
	public Taxi(String pIDTaxi, String pCompania)
	{
		idTaxi = pIDTaxi; 
		compania = pCompania; 
		serviciosTaxi = new ListaJSON<Servicio>(); 
		ingresos = 0;
		recorrido = 0;
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId()
	{
		return idTaxi;
	}

	/**
	 * @return company
	 */
	public String getCompany()
	{
		// TODO Auto-generated method stub
		return compania;
	}
	
	public ListaJSON<Servicio> darServiciostaxi()
	{
		return serviciosTaxi; 
	}
	
	public double totalIngresos()
	{
		ingresos = 0;
		serviciosTaxi.listing();
		
		Servicio actual = serviciosTaxi.getCurrent();
				
		while(actual != null)
		{
			ingresos += serviciosTaxi.getCurrent().getTripTotal();
			actual = serviciosTaxi.next().darActual(); 
		}
		return ingresos; 
	}
	
	public double totalRecorridos()
	{
		recorrido = 0;
		serviciosTaxi.listing();
		
		Servicio actual = serviciosTaxi.getCurrent();
				
		while(actual != null)
		{
			recorrido += serviciosTaxi.getCurrent().getTripTotal();
			actual = serviciosTaxi.next().darActual(); 
		}
		return recorrido; 
	}
	
	public Queue<Servicio> serviciosRango(RangoFechaHora rango)
	{
		Queue<Servicio> retorno = new Queue<Servicio>();
		
		String fInicial = rango.getFechaInicial()+"T"+rango.getHoraInicio();
		String fFinal = rango.getFechaFinal()+"T"+rango.getHoraFinal();
		
		Servicio buscado = new Servicio(fInicial, fFinal); 
		serviciosTaxi.listing(); 
		Servicio actual = serviciosTaxi.getCurrent();
		
		while(actual != null)
		{
			int a = actual.compareTo(buscado); 
			
			if(a == 0 || a == -1)
			{
				if(actual.FechaFinal().before(buscado.FechaFinal()))
					retorno.enqueue(serviciosTaxi.getCurrent()); 
			}
			serviciosTaxi.next();
			actual = serviciosTaxi.getCurrent(); 
		}
		return retorno; 
	}
	
	
	public int cuantosServiciosEnRango(RangoFechaHora rango)
	{
		return serviciosRango(rango).size(); 	 
	}
	
	
	public double IngresosRango(RangoFechaHora rango)
	{
		
		double ingresosRango = 0;
		Queue<Servicio> IRango = serviciosRango(rango);
		
		while(!IRango.isEmpty())
		{
			ingresosRango += IRango.dequeue().getTripTotal();
		}
		return ingresosRango; 
	}
	
	
	public double recorridosRango(RangoFechaHora rango)
	{
		double recorridoTotalRango = 0;
		
		Queue<Servicio> sServiciosRango = serviciosRango(rango);
		while(!sServiciosRango.isEmpty())
		{
			recorridoTotalRango += sServiciosRango.dequeue().getTripMiles();
		}
		return recorridoTotalRango;
	}
	
	
	public int tiempoTotalRango(RangoFechaHora rango)
	{
		int tiempo = 0;
		Queue<Servicio> sServicios = serviciosRango(rango);
		while(!sServicios.isEmpty())
		{
			tiempo += sServicios.dequeue().getTripSeconds(); 
		}
		return tiempo;
	}
	
	public double rentabilidad()
	{
		return totalIngresos()/totalRecorridos(); 
	}
	
	public int compareTo(Taxi o) 
	{
		// TODO Auto-generated method stub
		return 0;
	}	
}