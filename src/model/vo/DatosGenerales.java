package model.vo;

import model.data_structures.ListaJSON;

public class DatosGenerales 
{
	public ListaJSON<Compania> companias; 
	private ListaJSON<CompaniaServicios> serviciosCompania; 
	private ListaJSON<Servicio> servicios;
	
	public DatosGenerales()
	{
		companias = new ListaJSON<Compania>(); 
		serviciosCompania = new ListaJSON<CompaniaServicios>();
		servicios = new ListaJSON<Servicio>(); 
	}
	
	public ListaJSON<Compania> darCompanias()
	{
		return companias; 
	}
	
	public ListaJSON<CompaniaServicios> darServiciosCompania()
	{
		return serviciosCompania;
	}
	
	public ListaJSON<Servicio> darServicios()
	{
		return servicios; 
	}
	
	public Compania buscarCompania(String pCompania)
	{
		Compania retorno = null; 
		companias.listing();
		Compania a = companias.getCurrent(); 
		
		while(companias.getCurrent() != null) 
		{
			if(a.darNombre().equals(pCompania))
			{
				retorno = companias.getCurrent(); 
			}
			else
			{
				companias.next();
				companias.getCurrent(); 
			}
		}
		return retorno; 
	}
	
	public Taxi buscarTaxi(String pIDTaxi)
	{
		Taxi retorno = null; 
		companias.listing();
		Compania a = companias.getCurrent(); 
		
		while(a != null) 
		{
			
			if(a.buscarTaxi(pIDTaxi) != null)
			{
				retorno = companias.getCurrent().buscarTaxi(pIDTaxi);
			}
			else
			{
				companias.next();
				a = companias.getCurrent(); 
			}
		}
		return retorno; 
	}

	
}
