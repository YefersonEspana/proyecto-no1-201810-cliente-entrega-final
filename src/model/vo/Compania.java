package model.vo;

import model.data_structures.ListaJSON;

public class Compania implements Comparable<Compania> 
{
	/**
	 * 
	 */
	private String nombre;	
	
	/**
	 * 
	 */
	private ListaJSON<Taxi> taxisInscritos; 
	
	/**
	 * 
	 */
	private ListaJSON<Servicio> serviciosCompania;  
	
	/**
	 * 
	 * @param pNombre
	 */
	public Compania(String pNombre)
	{
		nombre = pNombre; 
		taxisInscritos = new ListaJSON<Taxi>();
		serviciosCompania = new ListaJSON<Servicio>();
	}
	
	public String darNombre() 
	{
		return nombre;
	}

	public void cambiarNombre(String nombre) 
	{
		this.nombre = nombre; 
	}

	public ListaJSON<Taxi> darTaxisInscritos() 
	{
		return taxisInscritos;
	}

	public void cambiarTaxisInscritos(ListaJSON<Taxi> taxisInscritos)
	{
		this.taxisInscritos = taxisInscritos;
	}
	
	public ListaJSON<Servicio> servicios()
	{
		return serviciosCompania; 
	}
	
	/**
	 * 
	 * @param pIdTaxi
	 * @return
	 */
	public Taxi buscarTaxi(String pIdTaxi)
	{
		Taxi retorno = null; 
		taxisInscritos.listing();
		Taxi actualT = taxisInscritos.getCurrent(); 
		
		while(actualT != null)
		{
			if(actualT.getTaxiId().equals(pIdTaxi))
			{
				retorno = taxisInscritos.getCurrent();
			}
			else
			{
			     taxisInscritos.next();
			     actualT = taxisInscritos.getCurrent(); 
			}
		}
		return retorno; 
	}
	
	public Taxi taxiMasRentable()
	{
		Taxi retorno = null; 
		taxisInscritos.listing();
		double max = 0;
		Taxi tActual = taxisInscritos.getCurrent(); 
		
		while(tActual != null) 
		{
			double a = tActual.rentabilidad(); 
			if(a>max)
			{
				retorno = tActual; 
				max = a; 
			}
			taxisInscritos.next(); 
			tActual = taxisInscritos.getCurrent(); 	
		}
		return retorno; 
		
	}
	
	public int compareTo(Compania o) 
	{
		return (nombre.compareTo(o.darNombre()) > 0 ? 1: nombre.compareTo(o.darNombre())  == 0 ? 0 : -1);
	}
}