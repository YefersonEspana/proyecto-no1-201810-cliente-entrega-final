package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	
	//TODO Ojo con este
	private String IDServicio;
	private String IDTaxi; 
	
	private double extras;
	private double fare;
	private String payment_type;
	private double tips;
	private double tolls;
	private double trip_miles;
	private int trip_seconds;
	private String trip_start_timestamp;
	private String trip_end_timestamp;
	private double trip_total;

	
	public Servicio(String fInicial, String fFinal)
	{
		trip_start_timestamp = fInicial; 
		trip_end_timestamp = fFinal;
	}
	
	public Servicio(String hInicial, String hFinal, double distancia, int duracion, double total, String IDServicio, String IDTaxi)
	{
		this.trip_start_timestamp = hInicial;
		this.trip_end_timestamp= hFinal;
		this.trip_miles = distancia;
		this.trip_seconds = duracion;
		this.trip_total = total;
		this.IDServicio = IDServicio; 
		this.IDTaxi = IDTaxi; 
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId()
	{
		return IDServicio;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{
		return IDTaxi;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() 
	{
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() 
	{
		return trip_total;
	}
	
	public Date FechaInicial()
	{
		Date inicio = null;
		try 
		{
			inicio = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(this.trip_start_timestamp);
		} 
		catch (ParseException e) 
		{
			System.out.println( "Error aplicando el formato de las fechas.");
		}
		return inicio;
	}
	
	public Date FechaFinal()
	{
		Date fFinal = null;
		try 
		{
			fFinal = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS" ).parse(this.trip_end_timestamp);
		} 
		catch (ParseException e) 
		{
			System.out.println( "Error aplicando el formato de las fechas.");
		}
		return fFinal;
	}

	public int compareTo(Servicio arg0) 
	{
		return 0;
	}
}
