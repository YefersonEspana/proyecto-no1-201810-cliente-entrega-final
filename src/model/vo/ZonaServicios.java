package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.ListaJSON;

public class ZonaServicios implements Comparable<ZonaServicios>
{

	private String idZona;	
	
	private ListaJSON<Servicio> fechasServicios;
	
	public ZonaServicios(String pIDZona)
	{
		idZona = pIDZona; 
		fechasServicios = new ListaJSON<Servicio>();
	}
	
	public String getIdZona() 
	{
		return idZona;
	}

	public void setIdZona(String idZona) 
	{
		this.idZona = idZona;
	}

	public LinkedList<Servicio> getFechasServicios() 
	{
		return fechasServicios;
	}

	public void setFechasServicios(ListaJSON<Servicio> fechasServicios) 
	{
		this.fechasServicios = fechasServicios;
	}

	public int compareTo(ZonaServicios o)
	{
		return(idZona.compareTo(o.idZona) > 0 ? 1: idZona.compareTo(o.idZona) == 0? 0: -1);
		
	}
}
