package model.data_structures;

public class Queue <T extends Comparable<T>> implements IQueue<T>
{

	private int size;
	private Node<T> cabeza; 
	private Node<T> ultimo;
	private Node<T> actual;
	

	public Queue()
	{
		cabeza = null;
		ultimo = null;
		actual = null; 
	}
	
	public void enqueue(T item) 
	{
		if(isEmpty())
		{
			cabeza = new Node<T>(item); 
			actual = cabeza;
			ultimo = cabeza;
		}
		else
		{
			actual = ultimo; 
			ultimo = new Node<T>(item);
			actual.cambiarSiguiente(ultimo); 
			actual = cabeza;
		}
		size++; 
	}

	public T dequeue() 
	{
		if(isEmpty())
			return null;
		else
		{
			T item = cabeza.darActual();
			cabeza = cabeza.darSiguiente();
			size--;
			return item;
		}
		
	}

	public void listing()
	{
		actual = cabeza;
	}
	
	public boolean isEmpty() 
	{
		if(cabeza == null)
			return true;
		else
			return false; 
	}

	public int size() 
	{
		return size;
	}

}
