package model.data_structures;

public class Stack <T extends Comparable<T>> implements IStack<T>

{
	private int size; 
	private Node<T> cabeza;
	private Node<T> actual;
	
	
	public Stack() 
	{
		cabeza = null;
		actual = cabeza;
	}
	
	public void push(T item) 
	{
		if(isEmpty())
		{
			cabeza = new Node<T>(item);
			actual = cabeza;
		}
		else
		{
			listing();
			cabeza = new Node<T>(item);
			cabeza.cambiarSiguiente(actual);
		}
		size++;
	}

	public T pop() 
	{
		if(isEmpty())
			return null; 
		T item = cabeza.darActual();
		cabeza = cabeza.darSiguiente(); 
		size--; 
		return item; 
	}
	
	public void listing()
	{
		actual = cabeza;
	}
	
	public boolean isEmpty() 
	{
		if(cabeza == null)
			return true;
		else
			return false; 
	}

	public int size() 
	{
		return size;
	}

}
