package model.data_structures;

public class ListaJSON <T extends Comparable<T>> implements LinkedList<T> 
{
	private Node<T> cabeza;
	
	private Node<T> actual; 
		
	private Node<T> ultimo;
	
	private int size; 
	
	/**
	 * 
	 */
	public ListaJSON()
	{
		cabeza = null; 
		actual = null;  
		ultimo = null;		
		size = 0; 
	}
	
	/**
	 * 
	 */
	public void add(T objeto) 
	{
		if (get(objeto) != null)
			System.out.println("El objeto ya se encuentra anadido."); 
		
		Node<T> nuevo = new Node<T>(objeto); 
		
		if (cabeza == null)
		{
			cabeza = nuevo;  			
			ultimo = nuevo; 
		}
		else 
		{
			ultimo.cambiarSiguiente(nuevo); 
		}
		size++; 
	}

	/**
	 * 
	 */
	//TODO Creo completo
	public void delete(T objeto) 
	{
		//Si hay al menos un objeto.
		if(size == 0)
			System.out.println("Error borrando datos."); 
		else
		{
			listing();
			
			//En caso del primer elemento.
			if(cabeza.darActual().compareTo(objeto) == 0)
			{
				//Solo exista un elemento.
				if(cabeza.darSiguiente() == null)
					cabeza.cambiarNodo(null); 
				
				//Mas de un elemento
				else
					cabeza = cabeza.darSiguiente(); 

				size--; 
			}
			
			else
			{
				while(actual.darSiguiente() != null)
				{
					if(actual.darSiguiente().darActual().compareTo(objeto) == 0)
					{
						actual.darSiguiente().cambiarSiguiente(actual.darSiguiente().darSiguiente()); 
								size--; 
					}
					else
						actual = actual.darSiguiente();
				}
				if(actual.darSiguiente() == null)
					System.out.println("No se encontro el objeto.");  
			}
		}

	}

	/**
	 * 
	 */
	public T get(T objeto) 
	{
		listing();
		while(actual != null)
		{
			if(actual.darActual().compareTo(objeto) == 0) 
			{
				return actual.darActual(); 
			}
			//TODO No completamente seguro.
		}
		return null; 
	}

	/**
	 * 
	 */
	public int size() 
	{
		return size;
	}

	/**
	 * 
	 */
	public T get(int a) 
	{
		listing(); 
		if((a + 1) > size) 
			return null; 
		while(actual.darSiguiente() != null && a != 0 )
		{
			actual = actual.darSiguiente();
			a--; 
		}
		return actual.darActual(); 
	}

	/**
	 * 
	 */
	public void listing() 
	{
		actual = cabeza; 
	}

	/**
	 * 
	 */
	public T getCurrent() 
	{
		return actual.darActual(); 
	} 
	
	/**
	 * 
	 */
	public Node<T> next() 
	{
		return actual.darSiguiente(); 
	}

	public boolean isEmpty() 
	{
		if(cabeza == null)
			return true;
		else
			return false; 
	}
}
