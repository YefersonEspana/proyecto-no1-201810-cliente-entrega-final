package model.data_structures;

public class Node <T>   
{
	/**
	 * 
	 */
	private T actual;
	
	/**
	 * 
	 */
	private Node<T> siguiente; 
	
	
	//TODO Aun sin definir	
//	private T anterior; 
	
	/**
	 * 
	 * @param objeto
	 */
	public Node(T objeto)
	{
		this.actual =  objeto;  
		siguiente = null; 
		
//		anterior = null; 
	}
	
	/**
	 * 
	 * @return
	 */
	public T darActual()
	{
		return this.actual; 
	}
	
	/**
	 * 
	 * @return
	 */
	public Node<T> darSiguiente()
	{
		return siguiente; 
	}  
	
	/**
	 * 
	 * @param nuevo
	 */
	public void cambiarNodo(T nuevo)
	{
		this.actual =  nuevo;    
	}
	
//	public Node<T> darAnterior()
//	{
//		return anterior; 
//	}
	
	public void cambiarSiguiente(Node<T> nuevo)
	{
		siguiente =  nuevo;   
	}

}
