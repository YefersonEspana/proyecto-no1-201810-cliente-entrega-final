package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.ListaJSON;
import model.data_structures.Queue;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.DatosGenerales;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	//TODO revisar
	private DatosGenerales todo; 

	//1C
	public boolean cargarSistema(String direccionJson) 
	{
		String dir="";

		if (direccionJson.equals(DIRECCION_SMALL_JSON))
			dir=DIRECCION_SMALL_JSON;
		if(direccionJson.equals(DIRECCION_MEDIUM_JSON))
			dir=DIRECCION_MEDIUM_JSON;
		if(direccionJson.equals(DIRECCION_LARGE_JSON))
			dir=DIRECCION_LARGE_JSON;

		JsonParser parser= new JsonParser();

		try 
		{
			JsonArray arr= (JsonArray) parser.parse(new FileReader(dir));
		
			///TODO no es seguro
			Compania compania = null;
			
			for (int i = 0; arr != null && i < arr.size(); i++ )
			{
				JsonObject obj= (JsonObject) arr.get(i); 

				String company="NaN";
				if(obj.get("company")!=null)
				{company=obj.get("company").getAsString();}
				//TODO Agregar las companias a una lista
				compania = todo.companias.get(new Compania(company)); 
				if(compania == null)
					todo.companias.add(new Compania(company)); 

				
				double dropoff_census_tract= 0;
				if(obj.get("dropoff_census_tract") != null)
				{dropoff_census_tract=obj.get("dropoff_census_tract").getAsDouble();}

				
				double dropoff_centroid_latitude = 0;
				if ( (obj.get("dropoff_centroid_latitude") != null))
				{ dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsDouble(); }

				
				JsonElement drop = obj.get("dropoff_centroid_location");
				String dropoff_type = drop == null? "NaN": drop.getAsJsonObject().get("type").getAsString();
				double droplat = drop == null? 0: drop.getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsDouble();
				double droplong = drop == null? 0: drop.getAsJsonObject().get("coordinates").getAsJsonArray().get(1).getAsDouble();
				
				
				double dropoff_centroid_longitude = 0;
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsDouble(); }

				
				int dropoff_community_area=0;
				if ( obj.get("dropoff_community_area") != null )
				{ dropoff_community_area = obj.get("dropoff_community_area").getAsInt(); }

				
				double extras = 0;
				if ( obj.get("extras") != null )
				{ extras = obj.get("extras").getAsDouble();}

				
				double fare=0;
				if ( obj.get("fare") != null )
				{ fare = obj.get("fare").getAsDouble(); }

				
				String payment_type="NaN";	
				if ( obj.get("payment_type") != null )
				{ payment_type = obj.get("payment_type").getAsString(); }

				
				double pickup_census_tract=0;
				if ( obj.get("pickup_census_tract") != null )
				{ pickup_census_tract = obj.get("pickup_census_tract").getAsDouble(); }

				
				double pickup_centroid_latitude=0;
				if ( obj.get("pickup_centroid_latitude") != null )
				{ pickup_centroid_latitude = obj.get("pickup_centroid_latitude").getAsDouble(); }

				
				JsonElement pickup_centroid_location = obj.get("pickup_centroid_location");
				String pickup_type = pickup_centroid_location == null? "NaN": pickup_centroid_location.getAsJsonObject().get("type").getAsString();
				double picklat = pickup_centroid_location == null? 0: pickup_centroid_location.getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsDouble();
				double picklong = pickup_centroid_location == null? 0: pickup_centroid_location.getAsJsonObject().get("coordinates").getAsJsonArray().get(1).getAsDouble();
				
				
				double pickup_centroid_longitude = 0;
				if ( obj.get("pickup_centroid_longitude") != null )
				{ pickup_centroid_longitude = obj.get("pickup_centroid_longitude").getAsDouble(); }

				
				String pickup_community_area="NaN";
				if ( obj.get("pickup_community_area") != null )
				{ pickup_community_area = obj.get("pickup_community_area").getAsString(); }

				
				String taxi_id="NaN";
				if ( obj.get("taxi_id") != null )
				{ taxi_id= obj.get("taxi_id").getAsString(); }
				//TODO agregar un taxi a una compania
				Taxi agregar = compania.darTaxisInscritos().get(new Taxi(taxi_id, company));
				if(agregar == null)
					compania.darTaxisInscritos().add(new Taxi(taxi_id, company)); 

				
				String trip_id = "NaN";
				if(obj.get("trip_id") == null)
				{trip_id = obj.get(trip_id).getAsString();}
				
				
				double tips=0;
				if ( obj.get("tips") != null )
				{ tips= obj.get("tips").getAsDouble(); }

				
				double tolls=0;
				if ( obj.get("tolls") != null )
				{ tolls= obj.get("tolls").getAsDouble(); }

				
				double trip_miles= 0;
				if ( obj.get("trip_miles") != null )
				{trip_miles= obj.get("trip_miles").getAsDouble();}

				
				int trip_seconds=0;
				if ( obj.get("trip_seconds") != null )
				{ trip_seconds= obj.get("trip_seconds").getAsInt(); }

				
				String trip_start_timestamp="NaN";
				if ( obj.get("trip_start_timestamp") != null )
				{ trip_start_timestamp= obj.get("trip_start_timestamp").getAsString(); }

				String trip_end_timestamp = "NaN"; 
				if ( obj.get("trip_end_timestamp") != null )
				{ trip_end_timestamp= obj.get("trip_end_timestamp").getAsString(); }
				
				double trip_total=0;
				if ( obj.get("trip_total") != null )
				{ trip_total= obj.get("trip_total").getAsDouble(); }

				
				JsonObject dropoff_localization_obj = null; 
				if ( obj.get("dropoff_centroid_location") != null )
				{ dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();

				
				JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();
				double longitude = dropoff_localization_arr.get(0).getAsDouble();
				double latitude = dropoff_localization_arr.get(1).getAsDouble();
				
				Servicio agregarServicio = new Servicio(trip_start_timestamp, trip_end_timestamp, trip_miles, trip_seconds, trip_total, trip_id, taxi_id);
				compania.servicios().add(agregarServicio); 
				agregar.darServiciostaxi().add(agregarServicio);
				todo.darServicios().add(agregarServicio); 
				}
			}
		}
		catch (JsonIOException e1 ) 
		{
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) 
		{
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) 
		{
			e3.printStackTrace();
		}
		catch (Exception e)
		{
			System.out.println("Cargar'"); 
		}
		return true;	
	}

	@Override //1A
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		Queue<Servicio> retorno = new Queue<Servicio>();

		String fInicial= rango.getFechaInicial()+"T"+rango.getHoraInicio();
		String fFinal= rango.getFechaFinal()+"T"+rango.getHoraFinal();
		
		Servicio parametro = new Servicio(fInicial, fFinal);
		ListaJSON<Servicio> servicios = todo.darServicios(); 
		
		servicios.listing();
		Servicio actual = servicios.getCurrent();
		while(actual != null)
		{
			int temp = actual.compareTo(parametro);
			if(temp <= 0)
			{
				if(actual.FechaFinal().before(parametro.FechaFinal())) 
					retorno.enqueue(servicios.getCurrent());
			}
			servicios.next();
			actual = servicios.getCurrent();
		}

		return retorno;
	}

	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //4A
	public LinkedList<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //1B
	public LinkedList<Compania> darCompaniasTaxisInscritos() 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		// TODO Auto-generated method stub
		return null;
	}

	//4B
	public LinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //2C
	public LinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //3C
	public LinkedList<CompaniaTaxi> taxisMasRentables()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}

}
